add_library(cuDNN::adv_infer SHARED IMPORTED)
set_target_properties(
	cuDNN::adv_infer PROPERTIES
	INTERFACE_INCLUDE_DIRECTORIES "/usr/include"
	IMPORTED_LOCATION "/usr/lib/@DEB_HOST_MULTIARCH@/libcudnn_adv_infer.so"
)
