add_library(cuDNN::ops_infer SHARED IMPORTED)
set_target_properties(
	cuDNN::ops_infer PROPERTIES
	INTERFACE_INCLUDE_DIRECTORIES "/usr/include"
	IMPORTED_LOCATION "/usr/lib/@DEB_HOST_MULTIARCH@/libcudnn_ops_infer.so"
)
