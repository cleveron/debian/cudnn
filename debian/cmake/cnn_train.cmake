add_library(cuDNN::cnn_train SHARED IMPORTED)
set_target_properties(
	cuDNN::cnn_train PROPERTIES
	INTERFACE_INCLUDE_DIRECTORIES "/usr/include"
	IMPORTED_LOCATION "/usr/lib/@DEB_HOST_MULTIARCH@/libcudnn_cnn_train.so"
)
